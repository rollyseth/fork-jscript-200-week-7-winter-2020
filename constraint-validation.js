// TODO
const selectEl=document.getElementById('contact-kind');


const setSelectValidity= function ()
{
    if(selectEl.value==='choose'){
        selectEl.setCustomValidity ('Must select an option');
        return; 
    }
    selectEl.setCustomValidity('');
    const businessEl= document.querySelector('.business');
    const techEl= document.querySelector('.technical');
       if(selectEl.value==='business')
       {
       // document.querySelector('.business').style.display='block';
       // document.querySelector('.technical').style.display='block';
      // document.querySelector('.business').classList.remove('hide');
      // document.querySelector('.technical').classList.add('hide');
      businessEl.classList.remove('hide');
      techEl.classList.add('hide');
      businessEl.querySelector('input').required=true;
      techEl.querySelector('input').required=false;
       }
       else
       {
       // document.querySelector('.business').style.display='none';
       // document.querySelector('.technical').style.display='none';
      // document.querySelector('.business').classList.add('hide');
      // document.querySelector('.technical').classList.remove('hide');
       businessEl.classList.add('hide');
       techEl.classList.remove('hide');
       businessEl.querySelector('input').required=false;
        techEl.querySelector('input').required=true;
        }
    }
      


selectEl.addEventListener('change',setSelectValidity);

//Local Storage
const saveChoice= function(e) {
 localStorage.setItem('contact-reason',this.value); 
}

selectEl.addEventListener('change',saveChoice);

const preselectedReason = localStorage.getItem('contact-reason');
if(preselectedReason){
    selectEl.value=preselectedReason; 
}

setSelectValidity(); 

const form=document.getElementById('connect-form');
form.addEventListener('submit', function(e){
    //checking if each item is valid with shared class name
    const inputs=document.getElementsByClassName('validate-input'); 

  let formIsValid=true; 
  Array.from(inputs).forEach(input=>
  {   console.log(input.checkValidity()); 
     input.checkValidity(); 
     // e.preventDefault(); 
      const small= input.parentElement.querySelector('small');
      if(!input.checkValidity())
      {     
            
            formIsValid=false;
            
            small.innerText=input.validationMessage;
            
            console.log(input.validationMessage);
      }
      else{
          small.innerText='';
        
          
      }
  //console.log(input.checkValidity());
  });
  if(!formIsValid){
  e.preventDefault(); 
  }
  
});
